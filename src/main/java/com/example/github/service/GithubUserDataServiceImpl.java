package com.example.github.service;

import com.example.github.config.ExeCode;
import com.example.github.config.RestException;
import com.example.github.entity.Request;
import com.example.github.mapper.GithubResponseMapper;
import com.example.github.model.GithubResponseDto;
import com.example.github.model.ResponseDto;
import com.example.github.repository.RequestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GithubUserDataServiceImpl implements GithubUserDataService {

    @Value("${github.url}")
    private String githubUrl;
    private final RequestRepository repository;
    private final RestTemplate restTemplate;

    private final GithubResponseMapper mapper;

    //can be moved to other service for example GithubDataService
    private GithubResponseDto fetchGithubData(String login) throws RestException {
        try {
            ResponseEntity<GithubResponseDto> entity = restTemplate.getForEntity(githubUrl + login, GithubResponseDto.class);
            return entity.getBody();
        } catch (Exception e) {
            throw new RestException(ExeCode.GITHUB_BAD_REQUEST, e.getMessage());
        }
    }


    @Transactional()
    public ResponseDto getUserData(String login) {
        updateRequestCount(login);
        return mapper.mapToResponse(fetchGithubData(login));
    }

    private void updateRequestCount(String login) {
        Request request = repository.findByLogin(login)
                .orElseGet(() -> Request.builder().login(login).requestCount(0L).build());

        request.setRequestCount(request.getRequestCount() + 1);
        repository.save(request);
    }
}
