package com.example.github.service;

import com.example.github.model.ResponseDto;

public interface GithubUserDataService {
    ResponseDto getUserData(String login);
}
