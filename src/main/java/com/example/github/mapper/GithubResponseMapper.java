package com.example.github.mapper;

import com.example.github.config.ExeCode;
import com.example.github.config.RestException;
import com.example.github.model.GithubResponseDto;
import com.example.github.model.ResponseDto;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class GithubResponseMapper {
    //refactor using mapstruct

    public ResponseDto mapToResponse(GithubResponseDto dto) {
        return ResponseDto.builder()
                .id(dto.getId())
                .type(dto.getType())
                .name(dto.getName())
                .avatarUrl(dto.getAvatarUrl())
                .createdAt(dto.getCreatedAt())
                .login(dto.getLogin())
                .calculations(prepareCalculations(dto.getFollowers(), dto.getPublicRepos()))
                .build();

    }

    private BigDecimal prepareCalculations(Long followers, Long publicRepos) {
        if (followers == null || followers == 0) {
            throw new RestException(ExeCode.FOLLOWERS_0);
        }
        BigDecimal followersBD = BigDecimal.valueOf(followers);
        BigDecimal publicReposBD = BigDecimal.valueOf(publicRepos);

        // 2 + liczba_public_repos
        BigDecimal reposSum = publicReposBD.add(BigDecimal.valueOf(2));

        // 6 / liczba_followers
        BigDecimal followersDivision = BigDecimal.valueOf(6)
                .divide(followersBD, 2, RoundingMode.HALF_UP);

        // Final calculation: (6 / liczba_followers) * (2 + liczba_public_repos)
        return followersDivision.multiply(reposSum);
    }
}
