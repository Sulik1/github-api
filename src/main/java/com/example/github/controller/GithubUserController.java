package com.example.github.controller;

import com.example.github.model.ResponseDto;
import com.example.github.service.GithubUserDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class GithubUserController {
    private final GithubUserDataService githubUserDataService;


    @GetMapping("{login}")
    public ResponseEntity<ResponseDto> getUser(@PathVariable String login) {
        return ResponseEntity.ok(githubUserDataService.getUserData(login));
    }
}
