package com.example.github.config;

import lombok.Getter;

@Getter
public class RestException extends RuntimeException {

    private final ExeCode exeCode;


    public RestException(ExeCode exeCode) {
        super(exeCode.getMessage());
        this.exeCode = exeCode;
    }

    public RestException(ExeCode exeCode, String msg) {
        super(msg);
        this.exeCode = exeCode;
    }
}