package com.example.github.config;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ExeCode {

    FOLLOWERS_0(HttpStatus.BAD_REQUEST, "Followers can not be 0"),
    GITHUB_BAD_REQUEST(HttpStatus.BAD_REQUEST, "Bad request");

    private final String message;
    private final HttpStatus httpStatus;

    ExeCode(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }
}
