package com.example.github.config;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(RestException.class)
    public ResponseEntity<ExeResponse> restExceptionHandler(RestException exception) {

        return new ResponseEntity<>(ExeResponse.builder()
                .message(exception.getMessage())
                .status(exception.getExeCode().getHttpStatus().value()).build(),
                exception.getExeCode().getHttpStatus());

    }




}
