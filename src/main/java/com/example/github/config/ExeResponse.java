package com.example.github.config;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Builder
@Getter
@Setter
public class ExeResponse {
    private int status;
    private String message;
}
