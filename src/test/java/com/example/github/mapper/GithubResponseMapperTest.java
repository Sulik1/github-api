package com.example.github.mapper;

import com.example.github.config.RestException;
import com.example.github.model.GithubResponseDto;
import com.example.github.model.ResponseDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GithubResponseMapperTest {
    private GithubResponseMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new GithubResponseMapper();
    }

    @Test
    void mapToResponse_ShouldCorrectlyMapFields() {
        GithubResponseDto dto = new GithubResponseDto();

        dto.setId(1L);
        dto.setType("User");
        dto.setName("Test User");
        dto.setAvatarUrl("http://example.com/avatar.jpg");
        dto.setCreatedAt(LocalDateTime.of(2000,2,3,2,20));
        dto.setLogin("testuser");
        dto.setFollowers(10L);
        dto.setPublicRepos(5L);


        ResponseDto responseDto = mapper.mapToResponse(dto);


        assertEquals(dto.getId(), responseDto.getId());
        assertEquals(dto.getType(), responseDto.getType());
        assertEquals(dto.getName(), responseDto.getName());
        assertEquals(dto.getAvatarUrl(), responseDto.getAvatarUrl());
        assertEquals(dto.getCreatedAt(), responseDto.getCreatedAt());
        assertEquals(dto.getLogin(), responseDto.getLogin());
        assertEquals(BigDecimal.valueOf(4.20).setScale(2), responseDto.getCalculations());

    }

    @Test
    void mapToResponse_ShouldThrowException() {
        GithubResponseDto dto = new GithubResponseDto();

        dto.setId(1L);
        dto.setType("User");
        dto.setName("Test User");
        dto.setAvatarUrl("http://example.com/avatar.jpg");
        dto.setCreatedAt(LocalDateTime.of(2000,2,3,2,20));
        dto.setLogin("testuser");
        dto.setFollowers(0L);
        dto.setPublicRepos(5L);

        assertThrows(RestException.class, () -> mapper.mapToResponse(dto));

    }
}
