package com.example.github.service;

import com.example.github.mapper.GithubResponseMapper;
import com.example.github.model.GithubResponseDto;
import com.example.github.model.ResponseDto;
import com.example.github.repository.RequestRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GithubUserDataServiceTest {
    //add integration tests, good to have github api response in resources to fetch data
    @Mock
    private RequestRepository repository;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private GithubResponseMapper mapper;

    @InjectMocks
    private GithubUserDataServiceImpl service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getUserData_ShouldReturnResponseDto_WhenCalled() {
        String login = "login";
        GithubResponseDto mockResponse = new GithubResponseDto();
        ResponseDto expectedDto = new ResponseDto();

        when(restTemplate.getForEntity(anyString(), eq(GithubResponseDto.class)))
                .thenReturn(ResponseEntity.ok(mockResponse));
        when(mapper.mapToResponse(any(GithubResponseDto.class)))
                .thenReturn(expectedDto);

        ResponseDto actualDto = service.getUserData(login);

        assertNotNull(actualDto);
        verify(repository, times(1)).findByLogin(login);
    }
}
